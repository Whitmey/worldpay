import com.fasterxml.jackson.databind.ObjectMapper;
import com.worldpay.model.MonetaryAmount;
import com.worldpay.model.ProductOffer;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.*;
import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;

public class AcceptanceTests {

    private final ObjectMapper mapper = new ObjectMapper();

    private static String formatUrl(String path) {
        return String.format("http://localhost:4567%s", path);
    }

    @Test
    public void create_offer() throws Exception {
        String expiryDate = LocalDateTime.of(2019, 3, 3, 4, 5, 5).toString();
        ProductOffer productOffer = new ProductOffer(
                null,
                "name",
                "description",
                expiryDate,
                null,
                new MonetaryAmount(TEN, "GBP")
        );

        given().body(mapper.writeValueAsString(productOffer))
                .when()
                .post(formatUrl("/productOffer"))
                .then()
                .statusCode(200);

        given()
                .when()
                .get(formatUrl("/productOffers"))
                .then()
                .statusCode(200)
                .body("[0].name", Matchers.equalTo("name"))
                .body("[0].description", Matchers.equalTo("description"))
                .body("[0].expiryDate", Matchers.equalTo(expiryDate))
                .body("[0].price.amount", Matchers.equalTo(10))
                .body("[0].price.currency", Matchers.equalTo("GBP"));

        post(formatUrl("/clearTestMemory"));
    }

    @Test
    public void fetch_individual_offer() throws Exception {
        String expiryDate = LocalDateTime.of(2019, 3, 3, 4, 5, 5).toString();
        ProductOffer productOffer = new ProductOffer(
                null,
                "name",
                "description",
                expiryDate,
                null,
                new MonetaryAmount(TEN, "GBP")
        );

        given().body(mapper.writeValueAsString(productOffer))
                .when()
                .post(formatUrl("/productOffer"))
                .then()
                .statusCode(200);

        String id = given().when()
                .get(formatUrl("/productOffers"))
                .then()
                .statusCode(200)
                .extract().jsonPath().getString("[0].id");


        ProductOffer response = given()
                .when()
                .get(formatUrl("/productOffers/" + id))
                .then()
                .statusCode(200)
                .extract().as(ProductOffer.class);

        assertEquals(response.getName(), "name");
        assertEquals(response.getDescription(), "description");
        assertEquals(response.getExpiryDate(), expiryDate);
        assertEquals(response.getPrice().getAmount(), TEN);
        assertEquals(response.getPrice().getCurrency(), "GBP");

        post(formatUrl("/clearTestMemory"));
    }

    @Test
    public void cancel_offer() throws Exception {
        ProductOffer productOffer = new ProductOffer(
                null,
                "name",
                "description",
                LocalDateTime.of(2019, 3, 3, 4, 5).toString(),
                null,
                new MonetaryAmount(TEN, "GBP")
        );

        given().body(mapper.writeValueAsString(productOffer))
                .when()
                .post(formatUrl("/productOffer"))
                .then()
                .statusCode(200);

        List<ProductOffer> productOffers = Arrays.asList(get(formatUrl("/productOffers"))
                .then()
                .statusCode(200)
                .extract().as(ProductOffer[].class));

        when()
                .put(formatUrl("/productOffer/" + productOffers.get(0).getId() + "/cancel"))
                .then()
                .statusCode(200);

        given()
                .when()
                .get(formatUrl("/productOffers"))
                .then()
                .statusCode(200)
                .body("[0].status", Matchers.equalTo("EXPIRED"));

        post(formatUrl("/clearTestMemory"));
    }


}
