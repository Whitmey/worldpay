package com.worldpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductOffer {

    private String id;
    private String name;
    private String description;
    private String expiryDate;
    private Status status;
    private MonetaryAmount price;

    public ProductOffer(String id, String name, String description, String expiryDate, Status status, MonetaryAmount price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.expiryDate = expiryDate;
        this.status = status;
        this.price = price;
    }

    public void validate() {
        if (name == null) {
            throw new IllegalArgumentException("Name must be provided");
        }
        if (description == null) {
            throw new IllegalArgumentException("Description must be provided");
        }
        if (expiryDate == null) {
            throw new IllegalArgumentException("Expiry date must be provided");
        }
        if (price == null) {
            throw new IllegalArgumentException("Price must be provided");
        }
    }

}
