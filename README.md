# WorldPay Offer Service

Description
----
A simple Java API that allows a merchant to create, query and cancel offers on products or services.

Docs
------
Endpoints

 - POST `/productOffer` - Create an offer.
    - Body:
            `name: String,
            description: String,
            expiryDate: String(LocalDateTime format),
            price: {
                amount: BigDecimal,
                currency: String
            }`
 - GET `/productOffers` - Fetch all offers.
 - GET `/productOffers/:productOfferId` - Fetch an individual offer.
 - PUT `/productOffer/:productOfferId/cancel` - Cancel an offer.

Testing
----
Acceptance tests and unit tests.