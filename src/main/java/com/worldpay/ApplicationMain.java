package com.worldpay;

import com.worldpay.model.ProductOffer;
import com.worldpay.repository.ProductOfferRepository;
import com.worldpay.service.ProductOfferService;
import com.worldpay.util.JsonTransformer;

import static spark.Spark.*;

public class ApplicationMain {

    public static void main(String[] args) {

        JsonTransformer jsonTransformer = new JsonTransformer();
        ProductOfferService service = new ProductOfferService(new ProductOfferRepository());

        post("/productOffer", (req, res) -> service.createOffer(jsonTransformer.fromJson(req.body(), ProductOffer.class)), jsonTransformer);

        get("/productOffers", (req, res) -> {
            res.type("application/json");
            return service.fetchOffers();
        }, jsonTransformer);

        get("/productOffers/:productOfferId", (req, res) -> {
            res.type("application/json");
            return service.fetchOffer(req.params(":productOfferId"));
        }, jsonTransformer);

        put("/productOffer/:productOfferId/cancel", (req, res) -> service.cancelOffer(req.params(":productOfferId")), jsonTransformer);

        post("/clearTestMemory", (req, res) -> service.clearTestMemory());

    }

}
