package com.worldpay.repository;

import com.worldpay.model.ProductOffer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ProductOfferRepository {

    private List<ProductOffer> offers = new ArrayList<>();

    public ProductOfferRepository() {}

    public void createOffer(ProductOffer offer) {
        offers.add(offer);
    }

    public List<ProductOffer> fetchOffers() {
        return offers;
    }

    public ProductOffer fetchOfferById(String id) {
        for (ProductOffer offer : offers) {
            if (id.equals(offer.getId())) {
                return offer;
            }
        }
        return null;
    }

    public void cancelOffer(String id) {
        for (ProductOffer offer : offers) {
            if (id.equals(offer.getId())) {
                offer.setExpiryDate(LocalDateTime.now().toString());
            }
        }
    }

    public void clearMemory() {
        offers = new ArrayList<>();
    }

}
