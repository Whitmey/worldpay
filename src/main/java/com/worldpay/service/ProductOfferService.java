package com.worldpay.service;

import com.worldpay.model.ProductOffer;
import com.worldpay.repository.ProductOfferRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static com.worldpay.model.Status.EXPIRED;
import static com.worldpay.model.Status.LIVE;
import static java.util.stream.Collectors.toList;

public class ProductOfferService {

    private final ProductOfferRepository productOfferRepository;

    public ProductOfferService(ProductOfferRepository repository) {
        this.productOfferRepository = repository;
    }

    public String createOffer(ProductOffer offer) {
        offer.validate();
        offer.setId(UUID.randomUUID().toString());
        productOfferRepository.createOffer(offer);
        return "SUCCESS";
    }

    public List<ProductOffer> fetchOffers() {
        List<ProductOffer> offers = productOfferRepository.fetchOffers();

        return offers.stream().map(productOffer -> {
            if (LocalDateTime.parse(productOffer.getExpiryDate()).isAfter(LocalDateTime.now())) {
                productOffer.setStatus(LIVE);
                return productOffer;
            }
            productOffer.setStatus(EXPIRED);
            return productOffer;
        }).collect(toList());
    }

    public ProductOffer fetchOffer(String id) throws IllegalArgumentException {
        ProductOffer productOffer = productOfferRepository.fetchOfferById(id);

        if (productOffer == null) {
            throw new IllegalArgumentException("Error: offer with that id does not exist");
        }

        return productOffer;
    }

    public String cancelOffer(String id) {
        productOfferRepository.cancelOffer(id);
        return "SUCCESS";
    }

    public String clearTestMemory() {
        productOfferRepository.clearMemory();
        return "SUCCESS";
    }

}
