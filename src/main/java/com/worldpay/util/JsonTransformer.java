package com.worldpay.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import spark.ResponseTransformer;

import java.io.IOException;

public class JsonTransformer implements ResponseTransformer {

    private static ObjectMapper mapper = new ObjectMapper();
    private Gson gson = new Gson();

    public String render(Object model) throws Exception {
        return gson.toJson(model);
    }

    public <T> T fromJson(String json, Class<T> classe) throws IOException {
        return gson.fromJson(json, classe);
    }

}
