import com.worldpay.model.MonetaryAmount;
import com.worldpay.model.ProductOffer;
import com.worldpay.repository.ProductOfferRepository;
import com.worldpay.service.ProductOfferService;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static com.worldpay.model.Status.EXPIRED;
import static com.worldpay.model.Status.LIVE;
import static java.math.BigDecimal.TEN;
import static java.util.Arrays.asList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UnitTests {

    private ProductOfferRepository repository = mock(ProductOfferRepository.class);
    private ProductOfferService service = new ProductOfferService(repository);

    private static ProductOffer productOffer = new ProductOffer(null,
            "name", "desc",
            LocalDateTime.now().plusYears(1).toString(),
            null, new MonetaryAmount(TEN, "GBP")
    );

    @Test
    public void testCreateOffer() {
        String responseMessage = service.createOffer(productOffer);

        Assert.assertEquals(responseMessage, "SUCCESS");
    }

    @Test
    public void testFetchOffersForLive() {
        when(repository.fetchOffers()).thenReturn(asList(productOffer));
        List<ProductOffer> productOffers = service.fetchOffers();

        Assert.assertEquals(productOffers.get(0).getStatus(), LIVE);
    }

    @Test
    public void testFetchOffersForExpired() {
        when(repository.fetchOffers()).thenReturn(asList(new ProductOffer(null,
                "name", "desc",
                LocalDateTime.now().minusYears(1).toString(),
                null, new MonetaryAmount(TEN, "GBP"))));
        List<ProductOffer> productOffers = service.fetchOffers();

        Assert.assertEquals(productOffers.get(0).getStatus(), EXPIRED);
    }

    @Test
    public void testFetchIndividualOffer() {
        when(repository.fetchOfferById("id")).thenReturn(productOffer);

        ProductOffer offer = service.fetchOffer("id");

        Assert.assertEquals(offer, productOffer);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testFetchIndividualOfferDoesNotExist() {
        when(repository.fetchOfferById("id")).thenReturn(null);

        service.fetchOffer("id");
    }

    @Test
    public void testCancelOffer() {
        String responseMessage = service.cancelOffer("id");

        Assert.assertEquals(responseMessage, "SUCCESS");
    }

    @Test(expected=IllegalArgumentException.class)
    public void testNameCannotBeNull() {
        service.createOffer(new ProductOffer(null, null, "desc", "date", null, new MonetaryAmount(TEN, "GBP")));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testDescriptionCannotBeNull() {
        service.createOffer(new ProductOffer(null, "name", null, "date", null, new MonetaryAmount(TEN, "GBP")));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testExpiryDateCannotBeNull() {
        service.createOffer(new ProductOffer(null, "name", "desc", null, null, new MonetaryAmount(TEN, "GBP")));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testPriceCannotBeNull() {
        service.createOffer(new ProductOffer(null, "name", "desc", "date", null, null));
    }

}
