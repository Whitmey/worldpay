package com.worldpay.model;

public enum Status {
    LIVE, EXPIRED
}
